package champollion;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Date;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class ChampollionJUnitTest {
	Enseignant untel;
	UE uml, java;
	Intervention icm; 


	@BeforeEach
	public void setUp() {
		untel = new Enseignant("untel", "untel@gmail.com");
		uml = new UE("UML");
		java = new UE("Programmation en java");		
		java = new UE("Programmation en java");	
		icm = new Intervention (new Salle("101",30),uml,untel,new Date(), 2,  TypeIntervention.CM);

	}

        @Test
        public void testHeuresPrevues(){
            untel.ajouteEnseignement(uml, 0, 1, 0);
            untel.ajouteEnseignement(java, 0, 1, 0);
            assertEquals(2, untel.heuresPrevues(),"L'enseignant doit réaliser 4h" );
        }
        
       
        @Test
        public void testHeuresPlanifiees(){
            Salle s = new Salle ("Salle 1", 25);
            Intervention e = new Intervention (s, uml, untel, new Date(), 2, TypeIntervention.TD);
            untel.ajouteInterventions(e);
            assertEquals(2, untel.heuresPlanifiees(), "L'enseignant doit réaliser 2h");
        }

        @Test 
        public void testEnSousService(){
            untel.ajouteEnseignement(uml, 1, 1, 1);
            assertEquals(true, untel.enSousService(), "L'enseignant est en sous service");

            untel.ajouteEnseignement(uml, 100, 50, 50);
            assertEquals(true, untel.enSousService(), "L'enseignant n'est pas en sous service");

        }
        
       
        
        @Test
        public void testNouvelEnseignantSansService() {
            assertEquals(0, untel.heuresPrevues(),
                            "Un nouvel enseignant doit avoir 0 heures prévues");
        }
        
        @Test
        public void tesAjouterIntervention(){
            untel.ajouteEnseignement(uml, 20, 20, 20); 
            untel.ajouteInterventions(icm); 
            assertEquals(1, untel.getInterventions().size()); 
        }
        
        
        
        
        @Test
        public void testAjouteHeures() {
                    // 10h TD pour UML
            untel.ajouteEnseignement(uml, 0, 10, 0);

            assertEquals(10, untel.heuresPrevuesPourUE(uml),
                            "L'enseignant doit maintenant avoir 10 heures prévues pour l'UE 'uml'");

                    // 20h TD pour UML
                    untel.ajouteEnseignement(uml, 0, 20, 0);

            assertEquals(10 + 20, untel.heuresPrevuesPourUE(uml),
                             "L'enseignant doit maintenant avoir 30 heures prévues pour l'UE 'uml'");

        }

}